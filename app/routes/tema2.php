<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'tema_nivel2' => array(
      array(
        'id_tema2' => "3",
        'desc_tema2' => "Cultura"
      ),
	  array(
        'id_tema2' => "4",
        'desc_tema2' => "Educacion"
      ),
	  array(
        'id_tema2' => "5",
        'desc_tema2' => "Hogares"
      ),
	  array(
        'id_tema2' => "6",
        'desc_tema2' => "Natalidad y fecundidad"
      ),
	  array(
        'id_tema2' => "7",
        'desc_tema2' => "Poblacion"
      ),
	  array(
        'id_tema2' => "8",
        'desc_tema2' => "Salud"
      ),
	  array(
        'id_tema2' => "9",
        'desc_tema2' => "Vivienda y urbanizacion"
      ),
      
    )
  );
  $app->render('tema2.php', $datos);
});
