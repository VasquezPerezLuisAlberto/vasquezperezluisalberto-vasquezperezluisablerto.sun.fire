<?php

$app->get('/xml', function() use($app) {

  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'entidad' => array(
      array(
        'id_entidad' => "1",
        'clave_entidad' => "000",
	'nombre_entidad' => "Nacional")) );

  $app->render('entidades.php', $datos);
});
