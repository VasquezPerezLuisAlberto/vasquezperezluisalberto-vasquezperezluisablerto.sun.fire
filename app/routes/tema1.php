<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'tema_nivel1' => array(
      array(
        'id_tema1' => "1",
        'desc_tema1' => "Poblacion"),
      array(
        'id_tema1' => "2",
        'desc_tema1' => "Sociedad")) );
  $app->render('tema1.php', $datos);

});
