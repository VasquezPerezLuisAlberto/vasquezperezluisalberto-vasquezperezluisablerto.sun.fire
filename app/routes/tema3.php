<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'tema_nivel3' => array(
      array(
        'id_tema3' => "10",
        'desc_tema3' => "Caracteristicas educativas de la poblacion"
      ),
	  array(
        'id_tema3' => "11",
        'desc_tema3' => "Caracteristicas culturales de la poblacion"
      ),
	  array(
        'id_tema3' => "12",
        'desc_tema3' => "Caracteristicas de las viviendas"
      ),
	  array(
        'id_tema3' => "13",
        'desc_tema3' => "Caracteristicas de los hogares"
      ),
	  array(
        'id_tema3' => "14",
        'desc_tema3' => "Derechohabiencia y uso de servicios"
      ),
	  array(
        'id_tema3' => "15",
        'desc_tema3' => "Distribucion por edad y sexo"
      ),
	  array(
        'id_tema3' => "16",
        'desc_tema3' => "Natalidad"
      ),
	  array(
        'id_tema3' => "17",
        'desc_tema3' => "Servicios y bienes en la vivienda"
      ),
	  array(
        'id_tema3' => "18",
        'desc_tema3' => "Volumen y crecimiento"
      ),
      
    )
  );
  $app->render('tema3.php', $datos);
});
